﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
/*
 Copyright (c) 2020 Sn00wConsoles.com/Ghostconsoles.com/ghostr/Bluesn00w
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software, And credit must be publicly
 shown in all copies of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
namespace bootPatch
{
    class Program
    {
        static void Log(string log)
        {
            Console.WriteLine(log);
        }

        static void Main(string[] args)
        {
            Log("  _                 _   _____      _       _     ");
            Log(" | |               | | |  __ \\    | |     | |    ");
            Log(" | |__   ___   ___ | |_| |__) |_ _| |_ ___| |__  ");
            Log(" | '_ \\ / _ \\ / _ \\| __|  ___/ _` | __/ __| '_ \\ ");
            Log(" | |_) | (_) | (_) | |_| |  | (_| | || (__| | | |");
            Log(" |_.__/ \\___/ \\___/ \\__|_|   \\__,_|\\__\\___|_| |_|");
            Log("");
            Log("bootPatch v1 by ghostr - https://sn00wconsoles.com");
            Log("\nPress any key to generate hacked bootanim");
            Console.Read();

            Log("Reading from Template");
            int Size = 0;
            byte[] TemplateData = File.ReadAllBytes("template.bin"); Size = Size + TemplateData.Length;
            Log("Reading from video.bin");
            byte[] VideoData = File.ReadAllBytes("video.bin"); Size = Size + VideoData.Length;
            Log("Generating padding data...");
            byte[] PaddingData = Enumerable.Repeat<byte>(0, 0x4167).ToArray(); Size = Size + PaddingData.Length;
            Log("Size is 0x" + Size.ToString("X2"));
            byte[] IOBuffer = new byte[Size];

            EndianIO dataStream = new EndianIO(IOBuffer, EndianStyle.BigEndian);

            dataStream.Writer.Write(TemplateData);
            Log("Wrote TemplateData\n");
            dataStream.Writer.Seek(0xF2C);
            dataStream.Writer.Write(VideoData.Length);
            Log("Wrote 0x4 at 0xF2C\n");

            dataStream.Writer.Seek(0x237000);
            dataStream.Writer.Write(VideoData);
            Log("Wrote 0x" + VideoData.Length.ToString("X2") + " at 0x237000\n");
            dataStream.Writer.Write(PaddingData);
            Log("Wrote 0x" + PaddingData.Length.ToString("X2") + " at " + (VideoData.Length + PaddingData.Length).ToString("X2") + "\n");

            File.WriteAllBytes("bootanim.xex", IOBuffer);
            dataStream.Close();
            Log("Done!");
            Thread.Sleep(5000);
        }
    }
}
